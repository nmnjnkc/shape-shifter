import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule, JwtService } from '@nestjs/jwt'; // add this import
import { jwtConstants } from './config/JWT/constants';

import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';

import { AuthGuard } from './auth/guards/auth.guard';
import { APP_GUARD } from '@nestjs/core';
import { MeasurementsModule } from './measurements/measurements.module';
import { ExerciseTypeModule } from './exercise-type/exercise-type.module';

import MongoDBConfig from './config/mongoDB/mongoDB.config';
import { MuscleGroupModule } from './exercise-type/muscle-group.module';

@Module({
  imports: [
    UsersModule,
    MongooseModule.forRootAsync({
      useFactory: async () => ({
        uri: MongoDBConfig.getConnectionString(),
      }),
    }),
    AuthModule,
    MuscleGroupModule,
    JwtModule.register({ secret: jwtConstants.secret }),
    MeasurementsModule,
    ExerciseTypeModule,
  ], 
  providers: [
    AuthGuard,
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    JwtService,
  ],
})
export class AppModule {}
