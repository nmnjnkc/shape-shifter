import { config } from 'dotenv';

config();

export default class MongoDBConfig {
  static getConnectionString(): string {
    return process.env.MONGO_DB_CONNECTION_STRING;
  }
}
