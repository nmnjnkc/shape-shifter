import {
  Controller,
  Body,
  Post,
  Get,
  Param,
  Patch,
  Delete,
  UseGuards,
} from '@nestjs/common';

import { Roles } from 'src/decorators/roles.decorator';
import { Role } from 'src/users/user.model';
import { RolesGuard } from 'src/auth/guards/roles.guard';

import { ExerciseTypeService } from '../Services/exercise-type.service';
import { CreateExerciseTypeDto } from '../dtos/create-exercise-type.dto';
import { UpdateExerciseTypeDto } from '../dtos/update-exercise-type.dto';
import { ExerciseType } from '../exercise-type.model';

@Roles(Role.USER)
@UseGuards(RolesGuard)
@Controller('exercise-type')
export class ExerciseTypeController {
  constructor(private exerciseTypeService: ExerciseTypeService) {}

  @Post()
  async addExerciseType(@Body() createExerciseTypeDto: CreateExerciseTypeDto): Promise<{id: string}> {
    const generatedId = await this.exerciseTypeService.insertExerciseType(
      createExerciseTypeDto,
    );

    return { id: generatedId };
  }

  @Get()
  async getAllExerciseTypes(): Promise<ExerciseType[]> {
    return await this.exerciseTypeService.getAllExerciseTypes();
  }

  @Get(':id')
  async getExerciseType(@Param('id') exerciseTypeId: string): Promise<ExerciseType> {
    return await this.exerciseTypeService.getSingleExerciseType(
      exerciseTypeId,
    );
  }

  @Patch(':id')
  async updateExerciseType(
    @Param('id') exerciseTypeId: string,
    @Body() updateExerciseTypeDto: UpdateExerciseTypeDto,
  ): Promise<void> {
    await this.exerciseTypeService.updateExerciseTypes(
      exerciseTypeId,
      updateExerciseTypeDto,
    );
  }

  @Delete(':id')
  async removeExerciseType(@Param('id') exerciseTypeId: string): Promise<void> {
    await this.exerciseTypeService.deleteExerciseTypes(exerciseTypeId);
  }
}

