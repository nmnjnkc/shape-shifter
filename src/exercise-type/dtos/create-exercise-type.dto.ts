import { IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class CreateExerciseTypeDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsMongoId({ each: true })
  @IsNotEmpty()
  muscleGroups: string[];
}
