import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Patch,
  UseGuards,
  Req,
  Delete,
} from '@nestjs/common';
import { MeasurementsService } from '../../measurements/services/measurements.service';
import { CreateMeasurementsDto } from '../../measurements/dto/createMeasurements.dto';
import { UpdateMeasurementsDto } from '../../measurements/dto/updateMeasurements.dto';
import { Measurements } from '../measurements.model';

import { Roles } from 'src/decorators/roles.decorator';
import { Role } from 'src/users/user.model';
import { RolesGuard } from 'src/auth/guards/roles.guard';

@Roles(Role.USER)
@UseGuards(RolesGuard)
@Controller('measurements')
export class MeasurementsController {
  constructor(private readonly measurementsService: MeasurementsService) {}

  @Post()
  async addMeasurements(
    @Body() createMeasurementsDto: CreateMeasurementsDto,
    @Req() request,
  ): Promise< { id: string } > {
    const generatedId = await this.measurementsService.insertMeasurements(
      createMeasurementsDto,
      request.user._id.toString(),
    );

    return { id: generatedId };
  }

  @Get()
  async getAllMeasurements():  Promise<Measurements[]> {
    return await this.measurementsService.getAllMeasurements();
  }

  @Get(':id')
  async getSingleMeasurements(@Param('id') measurementsId: string): Promise <Measurements> {
    return await this.measurementsService.getSingleMeasurements(measurementsId);
  }

  @Patch(':id')
  async updateMeasurements(
    @Param('id') measurementsId: string,
    @Body() updateMeasurementsDto: UpdateMeasurementsDto
  ): Promise<void> {
    await this.measurementsService.updateMeasurements(
      measurementsId,
      updateMeasurementsDto
    );
  }

  @Delete(':id')
  async deleteMeasurements(@Param('id') measurementsId: string): Promise<void> {
    await this.measurementsService.deleteMeasurements(measurementsId);
  }
}
