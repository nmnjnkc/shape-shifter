export const muscleGroups = [
    { muscleGroup: 'Upper Back' },
    { muscleGroup: 'Infraspinatus' },
    { muscleGroup: 'Middle Back' },
    { muscleGroup: 'Lower Back' },
    { muscleGroup: 'Deltoid' },
    { muscleGroup: 'Chest' },
    { muscleGroup: 'Abdominals' },
    { muscleGroup: 'Side Abs' },
    { muscleGroup: 'Biceps' },
    { muscleGroup: 'Triceps' },
    { muscleGroup: 'Forearms' },
    { muscleGroup: 'Gluteus Maximus' },
    { muscleGroup: 'Quadriceps' },
    { muscleGroup: 'Tibialis Anterior' },
    { muscleGroup: 'Hamstring Group' },
    { muscleGroup: 'Calfs' },
    
  ];