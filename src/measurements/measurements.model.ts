import * as mongoose from 'mongoose';

export const MeasurementsSchema = new mongoose.Schema({
  image: {
    type: String,
    required: [true, 'Image is required'],
  },
  weight: {
    type: Number,
    required: [true, 'Weight is required'],
    min: [0, 'Weight must be a positive number'],
  },
  chest: {
    type: Number,
    required: [true, 'Chest size is required'],
    min: [0, 'Chest must be a positive number'],
  },
  waist: {
    type: Number,
    required: [true, 'Waist size is required'],
    min: [0, 'Waist must be a positive number'],
  },
  hips: {
    type: Number,
    required: [true, 'Hip size is required'],
    min: [0, 'Hips must be a positive number'],
  },
  biceps: {
    type: Number,
    required: [true, 'Bicep size is required'],
    min: [0, 'Biceps must be a positive number'],
  },
  date: {
    type: Number,
    required: [true, 'Date is required'],
    min: [0, 'Date must be a positive number'],
  },
  user: {
    type: String,
    required: [true, ' User is required'],
  },
});

export interface Measurements extends mongoose.Document {
  id: string;
  image: string;
  weight: number;
  chest: number;
  waist: number;
  hips: number;
  biceps: number;
  date: number;
  user: string;
}
