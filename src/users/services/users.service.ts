import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { User } from '../user.model';
import { HashPassword } from 'src/utils/bcrypt';
import { Model } from 'mongoose';
import { UpdateUserDto } from '../dto/updateUser.dto';
import { CreateUserDto } from '../dto/createUser.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async insertUser(createUserDto: CreateUserDto): Promise<string> {
    const { name, email, password, role, gender, dateOfBirth, height } =
      createUserDto;

    await this.findByEmail(email);

    const hashedPassword = await HashPassword(password);
    const newUser = new this.userModel({
      name,
      email,
      password: hashedPassword,
      role,
      gender,
      dateOfBirth,
      height,
    });
    const result = await newUser.save();
    return result.id as string;
  }

  async getUsers(): Promise<User[]> {
    return await this.userModel.find().exec();
  }

  async getSingleUser(userId: string): Promise<User> {
    return await this.findUser(userId);
  }

  async updateUser(
    userId: string,
    updateUserDto: UpdateUserDto,
  ): Promise<void> {
    const updatedUser = await this.findUser(userId);

    updatedUser.name = updateUserDto.name || updatedUser.name;
    updatedUser.email = updateUserDto.email || updatedUser.email;
    updatedUser.role = updateUserDto.role || updatedUser.role;
    updatedUser.gender = updateUserDto.gender || updatedUser.gender;
    updatedUser.dateOfBirth =
      updateUserDto.dateOfBirth || updatedUser.dateOfBirth;
    if (updateUserDto.password) {
      const hashedPassword = await HashPassword(updateUserDto.password);
      updatedUser.password = hashedPassword;
    }
    updatedUser.height = updateUserDto.height || updatedUser.height;

    await updatedUser.save();
  }

  async deleteUser(userId: string): Promise<void> {
    const result = await this.userModel.deleteOne({ _id: userId }).exec();

    if (result.deletedCount === 0) {
      throw new NotFoundException('Couldn\'t find the user.');
    }
  }

  async findUser(id: string): Promise<User> {
    let user;
    try {
      user = await this.userModel.findById(id).exec();
    } catch (error) {
      throw new InternalServerErrorException('Error trying to fetch the user.');
    }
    if (!user) {
      throw new NotFoundException('Couldn\'t find the user.');
    }
    return user;
  }

  async findByEmail(email: string): Promise<void> {
    const user = await this.userModel.findOne({ email }).exec();
    if (user) {
      throw new NotFoundException('The user with this email already exists.');
    }
  }

  async findUserByEmail(email: string): Promise<User> {
    const user = await this.userModel.findOne({ email }).exec();
    if (!user) {
      throw new NotFoundException('The user with this email doesn\'t exists.');
    }
    return user;
  }
}
