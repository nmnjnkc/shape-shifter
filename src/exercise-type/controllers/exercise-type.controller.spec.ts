import { Test, TestingModule } from '@nestjs/testing';
import { ExerciseTypeController } from './exercise-type.controller';

describe('ExerciseTypeController', () => {
  let controller: ExerciseTypeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExerciseTypeController],
    }).compile();

    controller = module.get<ExerciseTypeController>(ExerciseTypeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
