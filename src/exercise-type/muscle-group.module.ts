import { Module } from '@nestjs/common';
import { MuscleGroupService } from './services/muscle-group.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MuscleGroupSchema } from './muscle-group.model';



@Module({
    imports:[
        MongooseModule.forFeature([
            { name: 'MuscleGroup', schema: MuscleGroupSchema},
          ]),
    ],
  controllers: [],
  providers: [MuscleGroupService]
})

export class MuscleGroupModule {}
 
