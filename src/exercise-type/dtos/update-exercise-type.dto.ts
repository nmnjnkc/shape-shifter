import { IsMongoId, IsOptional, IsString } from 'class-validator';

export class UpdateExerciseTypeDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsMongoId({ each: true })
  @IsOptional()
  muscleGroups: string[];
}
