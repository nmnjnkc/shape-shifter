import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/services/users.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtTokenService: JwtService,
  ) {}

  async validate(email: string, password: string): Promise<any> {
    const user = await this.usersService.findUserByEmail(email);

    if (user) {
      const match = await bcrypt.compare(password, user.password);
      if (match) {
        return user;
      }
    }
    return null;
  }

  async generateToken(user: any): Promise<{ access_token: string }> {
    const payload = {
      email: user._doc.email,
      sub: user._doc._id.toString(),
      role: user._doc.role,
    }; 

    return {
      access_token: this.jwtTokenService.sign(payload),
    };
  }
}
