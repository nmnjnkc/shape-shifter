import {
  Controller,
  Post,
  Body,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthService } from '../services/auth.service';
import { CreateUserDto } from 'src/users/dto/createUser.dto';
import { UsersService } from 'src/users/services/users.service';
import { Public } from 'src/decorators/public.decorator';

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private usersService: UsersService,
  ) {}

  @Post('/register')
  @Public()
  async register(@Body() createUserDto: CreateUserDto): Promise<{ id: string }> {
    const generatedId = await this.usersService.insertUser(createUserDto);
    return { id: generatedId };
  }

  @Post('/validate')
  @Public()
  async validate(@Body() credentials: { email: string; password: string }): Promise<{ access_token: string }> {
    const user = await this.authService.validate(
      credentials.email,
      credentials.password,
    );

    if (user) {
      return this.authService.generateToken(user);
    } else {
      throw new UnauthorizedException();
    }
  }
}
