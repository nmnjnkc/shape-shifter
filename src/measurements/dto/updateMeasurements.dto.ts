import { IsOptional, IsString, IsNumber, Min } from 'class-validator';

export class UpdateMeasurementsDto {
  @IsOptional()
  @IsString()
  image?: string;

  @IsOptional()
  @IsNumber()
  @Min(0)
  weight?: number;

  @IsOptional()
  @IsNumber()
  @Min(0)
  chest?: number;

  @IsOptional()
  @IsNumber()
  @Min(0)
  waist?: number;

  @IsOptional()
  @IsNumber()
  @Min(0)
  hips?: number;

  @IsOptional()
  @IsNumber()
  @Min(0)
  biceps?: number;

  @IsOptional()
  @IsNumber()
  @Min(0)
  date?: number;
}
