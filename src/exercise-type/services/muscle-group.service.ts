import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { MuscleGroup } from '../muscle-group.model';
import { muscleGroups } from '../muscle-group-data';

@Injectable()
export class MuscleGroupService {
  constructor(
    @InjectModel('MuscleGroup') private readonly muscleGroupModel: Model<MuscleGroup>,
  ) {}

  async saveMuscleGroups(): Promise<void> {
    await this.muscleGroupModel.insertMany(muscleGroups);
  }

  async loadMuscleGroups(): Promise<void> {
    await this.saveMuscleGroups();
  }

  async getMuscleGroups(): Promise<MuscleGroup[]> {
    return await this.muscleGroupModel.find().exec();
  }
}
