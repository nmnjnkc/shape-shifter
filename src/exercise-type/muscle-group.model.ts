import * as mongoose from 'mongoose';

export const MuscleGroupSchema = new mongoose.Schema({
  muscleGroup: {
    type: String,
  },
});

export interface MuscleGroup extends mongoose.Document {
    id: string;
    muscleGroup: string;
}
