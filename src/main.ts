import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { MuscleGroupService } from './exercise-type/services/muscle-group.service';

import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const muscleGroupService = app.get(MuscleGroupService);
  await muscleGroupService.saveMuscleGroups();
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(3000);
}

bootstrap();
