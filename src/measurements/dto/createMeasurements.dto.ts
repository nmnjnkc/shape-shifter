import { IsNotEmpty, IsString, IsNumber, Min } from 'class-validator';

export class CreateMeasurementsDto {
  @IsNotEmpty()
  @IsString()
  image: string;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  weight: number;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  chest: number;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  waist: number;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  hips: number;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  biceps: number;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  date: number;
}
