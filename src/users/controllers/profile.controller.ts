import {
  Controller,
  Body,
  Get,
  Param,
  Patch,
  Delete,
} from '@nestjs/common'; 

import { UsersService } from '../services/users.service';
import { UpdateUserDto } from '../dto/updateUser.dto';
import { User } from '../user.model';

@Controller('users/profile')
export class ProfileController {
  constructor(private usersService: UsersService) {}

  @Get()
  async getUser(@Param('id') userId: string): Promise<User> {
    return await this.usersService.getSingleUser(userId);
  }

  @Patch()
  async updateUser(
    @Param('id') userId: string,
    @Body() updateUserDto: UpdateUserDto
  ): Promise<void> {
    await this.usersService.updateUser(userId, updateUserDto);
  }

  @Delete()
  async removeUser(@Param('id') userId: string): Promise<void> {
    await this.usersService.deleteUser(userId);
  }
}
