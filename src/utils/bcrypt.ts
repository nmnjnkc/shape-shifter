import * as bcrypt from 'bcrypt';

export async function HashPassword(rawPassword: string): Promise<string> {
  const saltRounds = 10;
  const salt = await bcrypt.genSalt(saltRounds);
  return await bcrypt.hash(rawPassword, salt);
}
