import * as mongoose from 'mongoose';
import { MuscleGroup } from './muscle-group.model';

export const ExerciseTypeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Name is required'],
  },
  muscleGroups: [
    {
      type: mongoose.Types.ObjectId,
      ref: 'MuscleGroup',
      required: [true, 'Muscle Groups are required'],
    },
  ],
});

export interface ExerciseType extends mongoose.Document {
  id: string;
  name: string;
  muscleGroups: MuscleGroup['id'][];
}

