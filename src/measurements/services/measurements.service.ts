import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Measurements } from '../measurements.model';
import { CreateMeasurementsDto } from '../dto/createMeasurements.dto';
import { UpdateMeasurementsDto } from '../dto/updateMeasurements.dto';
import cloudinary from 'src/config/cloudinary/cloudinary.config';

@Injectable()
export class MeasurementsService {
  constructor(
    @InjectModel('Measurements')
    private readonly measurementsModel: Model<Measurements>,
  ) {}

  async insertMeasurements(
    createMeasurementsDto: CreateMeasurementsDto,
    userId: string
  ): Promise<string> {
    const { image, weight, chest, waist, hips, biceps, date } =
      createMeasurementsDto;

    const cloudinaryResult = await cloudinary.v2.uploader.upload(image);

    const newMeasurements = new this.measurementsModel({
      image: cloudinaryResult.secure_url,
      weight,
      chest,
      waist,
      hips,
      biceps,
      date,
      user: userId,
    });

    const result = await newMeasurements.save();
    return result.id as string;
  }

  async getAllMeasurements(): Promise<Measurements[]> {
    return await this.measurementsModel.find().exec();
  }

  async getSingleMeasurements(measurementsId: string): Promise<Measurements> {
    return await this.findMeasurements(measurementsId);
  }

  async updateMeasurements(
    measurementsId: string,
    updateMeasurementsDto: UpdateMeasurementsDto,
  ): Promise<void> {
    const updatedMeasurements = await this.findMeasurements(measurementsId);

    if (updateMeasurementsDto.image) {
      const cloudinaryResult = await cloudinary.v2.uploader.upload(
        updateMeasurementsDto.image,
      );
      updatedMeasurements.image = cloudinaryResult.secure_url;
    }

    updatedMeasurements.weight =
      updateMeasurementsDto.weight || updatedMeasurements.weight;
    updatedMeasurements.chest =
      updateMeasurementsDto.chest || updatedMeasurements.chest;
    updatedMeasurements.waist =
      updateMeasurementsDto.waist || updatedMeasurements.waist;
    updatedMeasurements.hips =
      updateMeasurementsDto.hips || updatedMeasurements.hips;
    updatedMeasurements.biceps =
      updateMeasurementsDto.biceps || updatedMeasurements.biceps;
    updatedMeasurements.date =
      updateMeasurementsDto.date || updatedMeasurements.date;

    await updatedMeasurements.save();
  }

  async deleteMeasurements(measurementsId: string): Promise<void> {
    const result = await this.measurementsModel
      .deleteOne({ _id: measurementsId })
      .exec();

    if (result.deletedCount === 0) {
      throw new NotFoundException('Couldn\'t find the measurements.');
    }
  }

  async findMeasurements(id: string): Promise<Measurements> {
    let measurements;
    try {
      measurements = await this.measurementsModel.findById(id).exec();
    } catch (error) {
      throw new InternalServerErrorException('Error trying to fetch the measurements.');
    }
    if (!measurements) {
      throw new NotFoundException('Couldn\'t find the measurements.');
    }
    return measurements;
  }
}
