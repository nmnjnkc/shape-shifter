import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose/dist';
import { UserSchema } from './user.model';

import { UsersController } from './controllers/users.controller';
import { UsersService } from './services/users.service';
import { AuthService } from 'src/auth/services/auth.service';
import { PassportModule } from '@nestjs/passport/dist';
import { JwtService } from '@nestjs/jwt';
import { ProfileController } from './controllers/profile.controller';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    PassportModule,
  ],
  controllers: [UsersController, ProfileController],
  providers: [UsersService, AuthService, JwtService],
  exports: [UsersService],
})
export class UsersModule {}
