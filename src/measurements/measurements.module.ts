import { Module } from '@nestjs/common';
import { MeasurementsController } from '../measurements/controllers/measurements.controller';
import { MeasurementsService } from './services/measurements.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MeasurementsSchema } from './measurements.model';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Measurements', schema: MeasurementsSchema },
    ]),
  ],
  controllers: [MeasurementsController],
  providers: [MeasurementsService],
})
export class MeasurementsModule {}
