import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ExerciseTypeController } from './controllers/exercise-type.controller';
import { ExerciseTypeService } from './Services/exercise-type.service';
import { ExerciseTypeSchema } from './exercise-type.model';
import { MuscleGroupSchema } from './muscle-group.model';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'ExerciseType', schema: ExerciseTypeSchema },
      { name: 'MuscleGroup', schema: MuscleGroupSchema }
    ]),
  ],
  controllers: [ExerciseTypeController],
  providers: [ExerciseTypeService],
})
export class ExerciseTypeModule {}
 