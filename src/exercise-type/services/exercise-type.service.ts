import { Injectable, NotFoundException, InternalServerErrorException, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateExerciseTypeDto } from '../dtos/create-exercise-type.dto';
import { UpdateExerciseTypeDto } from '../dtos/update-exercise-type.dto';
import { ExerciseType } from '../exercise-type.model';
import { MuscleGroup } from '../muscle-group.model';

@Injectable()
export class ExerciseTypeService {
  constructor(
    @InjectModel('ExerciseType')
    private readonly exerciseTypeModel: Model<ExerciseType>,
    @InjectModel('MuscleGroup')
    private readonly muscleGroupModel: Model<MuscleGroup>
  ) {}

  async insertExerciseType(exerciseTypeDto: CreateExerciseTypeDto): Promise<string>  {
    const { name, muscleGroups } = exerciseTypeDto;

    const existingMuscleGroup = await this.muscleGroupModel.find({ _id: { $in: muscleGroups } })

    if (existingMuscleGroup.length !== muscleGroups.length) {
      throw new BadRequestException('One or more muscle groups not found')
    }

    const newExerciseType = new this.exerciseTypeModel({ name, muscleGroups: existingMuscleGroup.map(muscleGroup => muscleGroup._id) });
    const result = await newExerciseType.save();
    return result.id as string;
  }

  async getAllExerciseTypes(): Promise<ExerciseType[]> {
    return await this.exerciseTypeModel.find().exec();
  }

  async getSingleExerciseType(exerciseTypeId: string): Promise<ExerciseType> {
    return await this.findExerciseType(exerciseTypeId);
  }

  async updateExerciseTypes(
    exerciseTypeId: string,
    updateExerciseTypeDto: UpdateExerciseTypeDto,
  ): Promise<void> {
    const updateExerciseType = await this.findExerciseType(exerciseTypeId);
    const { muscleGroups } = updateExerciseTypeDto;
  
    if (muscleGroups) {
      const existingMuscleGroup = await this.muscleGroupModel.find({ _id: { $in: muscleGroups } });
  
      if (existingMuscleGroup.length !== muscleGroups.length) {
        throw new BadRequestException('One or more muscle groups not found');
      }
  
      updateExerciseType.muscleGroups = existingMuscleGroup.map(muscleGroup => muscleGroup._id);
    }
  
    updateExerciseType.name = updateExerciseTypeDto.name || updateExerciseType.name;
  
    await updateExerciseType.save();
  }

  async deleteExerciseTypes(exerciseTypeId: string): Promise<void> {
    const result = await this.exerciseTypeModel
      .deleteOne({ _id: exerciseTypeId })
      .exec();
    if (!result) {
      throw new NotFoundException('Couldn\'t find the exercise type.');
    }
  }

  async findExerciseType(id: string): Promise<ExerciseType> {
    let exerciseType;
    try {
      exerciseType = await this.exerciseTypeModel.findById(id).exec();
    } catch (error) {
      throw new InternalServerErrorException('Error trying to fetch the exercise type.');
    }
    if (!exerciseType) {
      throw new NotFoundException('Couldn\'t find the exercise type.');
    }
    return exerciseType;
  }
}
