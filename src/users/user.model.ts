import * as mongoose from 'mongoose';

export enum Role {
  USER = 'user',
  ADMIN = 'admin',
}
 
export enum Gender {
  MALE = 'male',
  FEMALE = 'female',
  OTHER = 'other',
}

export const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Name is required'],
    minLength: [2, 'Name must be at least 2 characters long'],
    maxLength: [50, 'Name cannot exceed 50 characters'],
  },
  email: {
    type: String,
    required: [true, 'Email is required'],
    unique: true,
    validate: {
      validator: (value: string) =>
        /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value),
      message: 'Invalid email format',
    },
  },
  password: {
    type: String,
    required: [true, 'Password is required'],
    minLength: [8, 'Password must be at least 8 characters long'],
  },
  role: {
    type: String,
    required: [true, 'Role is required'],
    enum: [Role.USER, Role.ADMIN],
    default: Role.USER,
  },
  gender: {
    type: String,
    required: [true, 'Gender is required'],
    enum: [Gender.MALE, Gender.FEMALE, Gender.OTHER],
  },
  dateOfBirth: {
    type: Number,
    required: [true, 'Date of Birth is required'],
    min: [0, 'Date of Birth must be a positive number'],
  },
  height: {
    type: Number,
    required: [true, 'Height is required'],
    min: [0, 'Height must be a positive number'],
  },
});

export interface User extends mongoose.Document {
  id: string;
  name: string;
  email: string;
  password: string;
  role: string;
  gender: string;
  dateOfBirth: number;
  height: number;
}
